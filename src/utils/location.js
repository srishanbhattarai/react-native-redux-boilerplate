/**
 * Parses latitude, longitude and accuracy to provide a React Native MapView compatible region object.
 *
 * @param {number} latitude
 * @param {number} longitude
 * @param {number} accuracy
 * @returns {Object}
 */
export default (latitude, longitude, accuracy) => {
  const oneDegreeOfLongitudeInMeters = 111.32 * 1000
  const circumference = (40075 / 360) * 1000

  const latDelta = accuracy * (1 / (Math.cos(latitude) * circumference))
  const lonDelta = accuracy / oneDegreeOfLongitudeInMeters

  return {
    latitude,
    longitude,
    latitudeDelta: Math.max(0, latDelta),
    longitudeDelta: Math.max(0, lonDelta)
  }
}
