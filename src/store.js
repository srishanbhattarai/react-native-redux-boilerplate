import logger from 'redux-logger'
import { fromJS } from 'immutable'
import createSagaMiddleware from 'redux-saga'
import { combineReducers } from 'redux-immutable'
import { compose, applyMiddleware, createStore } from 'redux'

import langReducer from './containers/LanguageProvider/reducer'

/**
 * Returns a list of Redux middleware.
 * List of middleware:
 *  'redux-saga': Used for side-effect management
 *  'redux-logger': Enabled in dev mode for logging actions
 *
 * @returns {Array}
 */
const getMiddlewares = () => {
  // Base set of middleware
  const baseMiddleware = [createSagaMiddleware()]

  // Development mode middleware
  const devMiddleware = [logger]

  // eslint-disable-next-line no-undef
  if (__DEV__) {
    return [...baseMiddleware, ...devMiddleware]
  }

  return baseMiddleware
}

/**
 * Returns the root reducer of the application.
 *
 * @returns {Object}
 */
const createRootReducer = () =>
  // TODO: add reducers here
  combineReducers({
    lang: langReducer
  })

/**
 * Configure store initializes the Redux store with all the
 * necessary middleware and enhancers. The store object
 * is returned.
 *
 * @param {Object} initialState={}
 * @returns {Object}
 */
const configureStore = (initialState = {}) => {
  const enhancers = [applyMiddleware(...getMiddlewares())]
  const store = createStore(createRootReducer(), fromJS(initialState), compose(...enhancers))

  return store
}

export default configureStore
