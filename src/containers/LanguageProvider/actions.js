import { createAction } from 'redux-actions'

import { CHANGE_LOCALE } from './constants'

const changeLocale = createAction(CHANGE_LOCALE, locale => ({ locale }))

export default changeLocale
