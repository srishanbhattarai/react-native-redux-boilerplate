import 'intl'
import React from 'react'
import { Provider } from 'react-redux'

import App from './screens/App'
import configureStore from './store'
import translationMessages from './i18n'
import LangProvider from './containers/LanguageProvider'

// Initialize the Redux store
const store = configureStore({})

// Create a renderer that renders the root component
const createRenderer = messages => () => (
  <Provider store={store}>
    <LangProvider messages={messages}>
      <App />
    </LangProvider>
  </Provider>
)

export default createRenderer(translationMessages)
